local nvl = require('nvl')
local logger = nvl.logger{ prefix = 'gui.signature' }

local signature = {}

--- Show signature window.
-- @tparam {{string,...}} args
-- @tparam {number, number} active
-- @tparam {number, number} position
function signature.show(args, active)
  local broadcast_rpc = nvl.rpc{}
  assert(#active == 2)
  assert(type(active[1]) == 'number')
  assert(type(active[2]) == 'number')
  broadcast_rpc:notify('Gui', 'signature#show', args, active)
end

--- Hide signature window.
function signature.hide()
  local broadcast_rpc = nvl.rpc{}
  broadcast_rpc:notify('Gui', 'signature#hide')
end

--- Set style for signature window.
-- @tparam {fg=string,bg=string,active_sig_bg=string,active_param_fg=string}
--  style
function signature.set_style(style)
  local broadcast_rpc = nvl.rpc{}
  broadcast_rpc:notify('Gui', 'signature#set_style', style)
end

return signature
