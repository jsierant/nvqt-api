local nvl = require('nvl')

local function set_completion_kind_style(style)
  local broadcast_rpc = nvl.rpc{}
  broadcast_rpc:notify('Gui', 'completionmenu#set_kind_style', style)
end

return {
  signature = require('gui.signature'),
  popup = require('gui.popup'),
  problems_popup = require('gui.problems_popup'),
  set_completion_kind_style = set_completion_kind_style
}
