local nvl = require('nvl')
local logger = nvl.logger{ prefix = 'gui.popup' }

local popup = {}
popup.window_expansion_direction = { above='above', below='below' }

local visible = false
local broadcast_rpc = nvl.rpc{}

-- @tparam {{string|SyledItem,...},...} data
-- @tparam {line=number,col=number,dir=popup.window_expansion_direction}
-- StyledItem = {text=string, hl={guifg[opt]=string, guibg[opt]=string}}
-- @tparam[opt] {grid=string,shadow=string}
function popup.show(data, position, decoration)
  visible = true
  logger:debug('show: %s', data)
  broadcast_rpc:notify('Gui', 'popup#show', data, position, decoration)
end

function popup.update(data)
  if not visible then
    logger:warn("attempting to update not visible popup window")
    return
  end
  logger:debug('update: %s', data)
  broadcast_rpc:notify('Gui', 'popup#update', data)
end

function popup.update_style(style)
  logger:debug('update style: %s', style)
  broadcast_rpc:notify('Gui', 'popup#update_style', style)
end

-- @tparam {{string,...},...} data
function popup.hide()
  if not visible then return end
  logger:debug('hide')
  broadcast_rpc:notify('Gui', 'popup#hide')
  visible = false
end

return popup
