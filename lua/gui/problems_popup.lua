local nvl = require('nvl')
local diag = require('galen')
local popup = require("gui.popup")
local theme = require("darktooth")

local problems_popup = {}

local bgcolors = {}
bgcolors[nvl.problem.severities.error] = theme.palette.faded_red
bgcolors[nvl.problem.severities.warning] = theme.palette.faded_yellow
bgcolors[nvl.problem.severities.info] = theme.palette.faded_green
bgcolors[nvl.problem.severities.hint] = theme.palette.faded_green

local function bg(severity)
  return bgcolors[severity] or theme.palette.dark2
end

function problems_popup.show(buf)
  local problems = diag.storage.get_line_problems(buf, nvl.cursor.line())
  if #problems == 0 then return problems_popup.hide() end

  popup.show(nvl.utils.table.map(
    function(p)
      return {
        {text=p.source, hl={guibg=theme.palette.dark2}}, p.msg,
        {text=nvl.problem.str_severity(p.severity),
         hl = {guibg=bg(p.severity)}}
      }
    end,
    problems),
  {dir='above'},
  {shadow='top_left', grid='full'}
  )
end

problems_popup.hide = popup.hide

nvl.mode.on_signal(nvl.mode.signals.insert_enter, problems_popup.hide)

return problems_popup
